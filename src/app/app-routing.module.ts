import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './components/about/about.component';
import { HomeComponent } from './components/home/home.component';
import { PricesComponent } from './components/prices/prices.component';
import { Usuario20Component } from './components/prices/usuario20/usuario20.component';
import { Usuario50Component } from './components/prices/usuario50/usuario50.component';
import { Usuario80Component } from './components/prices/usuario80/usuario80.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'prices/:id', 
  component:PricesComponent,
  children:[
    { path: 'usuario20', component: Usuario20Component},
    { path: 'usuario50', component: Usuario50Component},
    { path: 'usuario80', component:Usuario80Component},
    { path:'**',pathMatch:'full', redirectTo:'usuario20'}
  ]},

  { path: 'about', component:AboutComponent},
  { path:'**',redirectTo:'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
