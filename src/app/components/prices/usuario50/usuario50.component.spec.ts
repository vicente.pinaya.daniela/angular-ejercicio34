import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Usuario50Component } from './usuario50.component';

describe('Usuario50Component', () => {
  let component: Usuario50Component;
  let fixture: ComponentFixture<Usuario50Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Usuario50Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Usuario50Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
